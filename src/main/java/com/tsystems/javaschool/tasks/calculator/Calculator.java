package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Stack;

public class Calculator {
    /**
     *
     * @author Amir Kutyev
     *
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private Stack<Character> stack = new Stack<>();
    private StringBuilder stringBuilder = new StringBuilder();

    public String evaluate(String statement) {
        if (statement == null ||statement== "") {
            return null;
        }
        StringBuilder number = new StringBuilder();
        StringBuilder exp;

        if ((isOperation(statement.charAt(0)) || ((exp = infixToPostfix(statement)) == null)))
            return null;

        Stack<String> numStack = new Stack<>();
        for (char c : exp.toString().toCharArray()) {
            if (Character.isDigit(c) || c == '.') {
                number.append(c);
            } else if (c == ' ') {
                numStack.push(number.toString());
                number.delete(0, number.length());
            } else if (isOperation(c)) {
                double op2 = Double.parseDouble(numStack.pop());
                double op1 = Double.parseDouble(numStack.pop());
                double res;
                switch (c) {
                    case '*':
                        res = op1 * op2;
                        numStack.push(String.valueOf(res));
                        break;

                    case '/':
                        res = op1 / op2;
                        if (Double.compare(op1, 0) == 0 || Double.compare(op2, 0) == 0)
                            return null;
                        numStack.push(String.valueOf(res));
                        break;

                    case '+':
                        res = op1 + op2;
                        numStack.push(String.valueOf(res));
                        break;

                    case '-':
                        res = op1 - op2;
                        numStack.push(String.valueOf(res));
                        break;
                }
            }

        }
        double finalResult = Double.parseDouble(numStack.pop());
        DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.US);
        DecimalFormat df = new DecimalFormat("#.####", dfs);
        String outputResult = df.format(finalResult);
        return outputResult;
    }

    private boolean isOperation(char symbOfStatement) {
        switch (symbOfStatement) {
            case '+':
            case '-':
            case '*':
            case '/':
                return true;
        }
        return false;
    }

    private StringBuilder infixToPostfix (String string) {
        int numOfOpBr = 0;
        int numOfClBr = 0;
        char prevC = '?';
        for (char c : string.toCharArray()) {
            if (c == prevC)
                return null;
            if (stringBuilder.length() > 0)
                if (!Character.isDigit(c) && c != '.')
                    if (stringBuilder.charAt(stringBuilder.length() - 1) != ' ' && !isOperation(stringBuilder.charAt(stringBuilder.length() - 1)))
                        stringBuilder.append(" ");
            switch (c) {
                case '+':
                case '-':
                    parseOp(c, 1);
                    prevC = c;
                    break;

                case '/':
                case '*':
                    parseOp(c, 2);
                    prevC = c;
                    break;

                case '(':
                    numOfOpBr++;
                    stack.push(c);
                    break;

                case ')':
                    numOfClBr++;
                    closeBracket();
                    break;

                default:
                    if (Character.isDigit(c) || c == '.')
                        stringBuilder.append(c);
                    else
                        return null;
                    if (c == '.')
                        prevC = c;
                    break;
            }
        }
        if (numOfOpBr != numOfClBr)
            return null;
        if (!isOperation(stringBuilder.charAt(stringBuilder.length() - 1)))
            stringBuilder.append(" ");
        while (!stack.isEmpty())
            stringBuilder.append(stack.pop());

        System.out.println(stringBuilder.toString());
        return stringBuilder;
    }

    private void parseOp(char c, int priority) {
        while (!stack.isEmpty()) {
            char onTop = stack.pop();
            if (onTop == '(') {
                stack.push(onTop);
                break;
            } else {
                int priorityPrv;
                if (onTop == '-' || onTop == '+')
                    priorityPrv = 1;
                else
                    priorityPrv = 2;
                if (priorityPrv >= priority)
                    stringBuilder.append(onTop);
                else {
                    stack.push(onTop);
                    break;
                }
            }
        }
        stack.push(c);
    }

    private void closeBracket() {
        while (!stack.isEmpty()) {
            char ch = stack.pop();
            if (ch == '(')
                break;
            else
                stringBuilder.append(ch);
        }
    }

}
