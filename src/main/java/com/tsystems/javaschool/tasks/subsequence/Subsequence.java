package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     *
     * @author Amir Kutyev
     *
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */


    public static boolean find(List x, List y) {
        Object temp1;
        Object temp2;

        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        Iterator iterator1 = x.iterator();
        Iterator iterator2 = y.iterator();

        if (x.size() == 0) {
            System.out.println("Of course it can be created. It's empty!");
            return true;
        }

        if (y.size() == 0) {
            System.out.println("Of course you cannot create anything from it. It's empty!");
            return false;
        }

        while (iterator1.hasNext()) {
            temp1 = iterator1.next();
            while (iterator2.hasNext()) {
                temp2 = iterator2.next();
                if ((iterator2.hasNext() == false) && (iterator1.hasNext() == true)) return false;
                if ((iterator2.hasNext() == false) && (iterator1.hasNext() == false)) {
                    return temp1.equals(temp2) ? true : false;
                }
                if (temp1.equals(temp2)) break;
            }
        }
        return true;
    }
}
