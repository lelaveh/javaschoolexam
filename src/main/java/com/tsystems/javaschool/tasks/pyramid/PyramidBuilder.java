package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * @author Amir Kutyev
     *
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public static int[][] buildPyramid(List<Integer> inputNumbers) {
        Iterator<Integer> iterator = inputNumbers.iterator();
        int sum = 1;
        int calcHeight = 0;
        for (int i = 2, j = 0; sum < inputNumbers.size(); i++) {
            calcHeight = sum;
            sum += i;
        }

        if (sum != inputNumbers.size()) throw new CannotBuildPyramidException("Cannot create a pyramid with this number of elements");
        for (Object i : inputNumbers)
            if (i == null) throw new CannotBuildPyramidException("One of the elements was not initialized");

        try {
            Collections.sort(inputNumbers);
        } catch (OutOfMemoryError e) {
            throw new CannotBuildPyramidException("Too many objects");
        }

        int height = inputNumbers.size() - calcHeight;
        int width = (height * 2) - 1;

        int[][] pyramid = new int[height][width];
        int firstElement = width / 2;


        for (int i = 0; i < height; i++, firstElement -= 1) {
            for (int countOfElements = 0, step = 0; countOfElements <= i; countOfElements += 1, step += 2) {
                pyramid[i][firstElement + step] = iterator.next();
            }
        }
        return pyramid;
    }


}
